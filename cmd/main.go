package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/prometheus/common/log"
	"gitlab.com/beabys/img-service/pkg/config"
)

type Application struct {
	Config   *config.Config
	Response *Response
}

type Response struct {
	StatusCode int
	Content    *ResponseContent
}

type ResponseContent struct {
	Msg  interface{} `json:"message"`
	Data interface{} `json:"data"`
}

type ImageData struct {
	Data   bytes.Buffer
	Format string
}

func main() {
	config := config.LoadConfig()
	app := Application{
		Config: config,
	}
	r := chi.NewRouter()
	// add middleware for log requests
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.NotFound(app.notFound)
	r.Get("/favicon.ico", faviconHandler)
	r.Route("/image", func(r chi.Router) {
		r.Use(validateParams)
		r.Post("", app.postImage)
		r.Post("/", app.postImage)
		// r.Get("/", app.getImage)
	})
	log.Infof("Starting Server on port: %v", app.Config.App.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", app.Config.App.Port), r))
}

//serve favicon
func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/favicon.ico")
}

// not found handler middleware
func (app *Application) notFound(w http.ResponseWriter, r *http.Request) {
	content := ResponseContent{"Not Found", ""}
	response := Response{404, &content}
	app.Response = &response
	app.responseWritter(w)
	return
}

func (app *Application) postImage(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	content := ResponseContent{"", ""}
	response := Response{200, &content}
	app.Response = &response
	uuid, err := getIDs(app.Config)
	if err != nil {
		log.Infof("error: %v", err.Error)
		errorResponse(w)
		return
	}
	dataImage, ok := ctx.Value("image_content").(*ImageData)
	if !ok {
		log.Infof("error: %v", err.Error)
		errorResponse(w)
		return
	}
	currentTime := time.Now()
	fileName := fmt.Sprintf("%v.%s", uuid, dataImage.Format)
	pathWithFile := fmt.Sprintf("/%s/%s", currentTime.Format("2006/01/02"), fileName)
	fullPathWithFile := fmt.Sprintf("%s%s", app.Config.App.CommonPath, pathWithFile)
	//verify folder exist
	err = ensureDir(fmt.Sprintf("%s/%s", app.Config.App.CommonPath, currentTime.Format("2006/01/02")))
	if err != nil {
		log.Infof("error: %v", err.Error)
		errorResponse(w)
		return
	}
	// creates the file
	f, err := os.Create(fullPathWithFile)
	if err != nil {
		log.Infof("error: %v", err.Error)
		errorResponse(w)
		return
	}
	defer f.Close()
	data := dataImage.Data.Bytes()
	if _, err := f.Write(data); err != nil {
		log.Infof("error: %v", err.Error)
		errorResponse(w)
		return
	}
	if err := f.Sync(); err != nil {
		log.Infof("error: %v", err.Error)
		errorResponse(w)
		return
	}
	app.Response.Content.Data = pathWithFile
	app.responseWritter(w)
	return
}

// Create Folder
func ensureDir(dirName string) error {
	err := os.MkdirAll(dirName, 0755)
	if err == nil || os.IsExist(err) {
		return nil
	} else {
		return err
	}
}

// Get Id's from leaf master
func getIDs(config *config.Config) (int64, error) {
	var uuid int64
	leafIDsURL := fmt.Sprintf("%v/%v/1", config.LeafIDs.URL, config.LeafIDs.Path)
	resp, err := http.Get(leafIDsURL)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	// create generic map to convert the response without any struct defined
	var result map[string]interface{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		return 0, err
	}
	data, ok := result["data"].([]interface{})
	if !ok {
		return 0, errors.New("invalid response when try to generate uuids from leafIDs")
	}
	for _, uuid := range data {
		uuid, err := strconv.ParseInt(uuid.(string), 10, 64)
		if err != nil {
			return 0, err
		}
		return uuid, nil
	}
	return uuid, nil
}

// middleware to validate the request
func validateParams(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data := r.FormValue("code_base64")
		if data == "" {
			log.Info("no code_base64 on request body")
			errorResponse(w)
			return
		}
		idx := strings.Index(data, ";base64,")
		if idx < 0 {
			log.Info("no code_base64 on request body")
			errorResponse(w)
			return
		}
		reader := base64.NewDecoder(base64.StdEncoding, strings.NewReader(data[idx+8:]))
		buff := bytes.Buffer{}
		_, err := buff.ReadFrom(reader)
		if err != nil {
			log.Info("error reading base 64 file")
			errorResponse(w)
			return
		}
		_, fm, err := image.DecodeConfig(bytes.NewReader(buff.Bytes()))
		if err != nil {
			log.Info("error decoding base_64 file")
			errorResponse(w)
			return
		}
		imageData := ImageData{
			buff,
			fm,
		}
		ctx := r.Context()
		ctx = context.WithValue(ctx, "image_content", &imageData)
		next.ServeHTTP(w, r.WithContext(ctx))
		return
	})
}

// response writter to set status code and response body
func (app *Application) responseWritter(w http.ResponseWriter) {
	w.WriteHeader(app.Response.StatusCode)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	json.NewEncoder(w).Encode(app.Response.Content)
}

// response writter to set status code and response body for error
func errorResponse(w http.ResponseWriter) {
	w.WriteHeader(404)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	fmt.Fprintf(w, "")
}
