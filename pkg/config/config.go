package config

import (
	"log"

	"github.com/spf13/viper"
)

// Config is a struct define configuration for the app and list of DSP
type Config struct {
	App     App    `mapstructure:"app"`
	LeafIDs LeafID `mapstructure:"leaf_ids"`
}

// App is a struct to define configurations for the app
type App struct {
	Port       int    `mapstructure:"port"`
	CommonPath string `mapstructure:"file_path"`
}

// LeafID is a struct to define configurations for Leaf Master
type LeafID struct {
	URL     string `mapstructure:"url_root"`
	Path    string `mapstructure:"id_path"`
	Timeout int    `mapstructure:"timeout_seconds"`
}

// LoadConfig is a function to load the configuration, stored on the config files
func LoadConfig() *Config {
	var config Config
	// set config file name, path and file type
	viper.AddConfigPath("./configs")
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Fail to load configs: %s", err)
	}

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("Unable to decode application configs: %s", err)
	}

	// Enable VIPER to read Environment Variables
	viper.AutomaticEnv()
	// assign Port for env
	port := viper.GetInt("APPLICATION_PORT")
	config.App.Port = port
	return &config
}
