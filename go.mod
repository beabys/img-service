module gitlab.com/beabys/img-service

go 1.13

require (
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/prometheus/common v0.9.1
	github.com/spf13/viper v1.6.2
)
